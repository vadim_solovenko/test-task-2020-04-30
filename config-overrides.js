module.exports = (config) => {
  config.module.rules.forEach((el) => {
    if (!!el.oneOf) {
      el.oneOf.forEach((ell) => {
        Array.isArray(ell.use) &&
          ell.use.forEach((elll) => {
            if (elll.loader === require.resolve("css-loader")) {
              elll.loader = require.resolve("astroturf/css-loader");
            }
          });
      });
    }
  });

  config.module.rules = [
    ...config.module.rules,

    {
      test: /\.(js|mjs|jsx|ts|tsx)$/,
      use: [
        {
          loader: "astroturf/loader",
          options: { extension: ".module.css" },
        },
      ],
    },
  ];
  return config;
};
