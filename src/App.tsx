import {createBrowserHistory} from "history";
import React, {Suspense} from "react";
import {Provider} from "react-redux";
import {Router} from "react-router";
import {PersistGate} from "redux-persist/integration/react";
import "./App.css";
import {Tasks} from "./pages/tasks/Tasks";
import {configureStore} from "./store/configureStore";

export const history = createBrowserHistory();
const {store, persistor} = configureStore(() => {
  //
}, history);

function App() {
  return (
    <Router history={history}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Suspense fallback={null}>
            <Tasks />
          </Suspense>
        </PersistGate>
      </Provider>
    </Router>
  );
}

export default App;
