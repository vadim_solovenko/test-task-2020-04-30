import querystring from "query-string";

export type IHttpMethods = "GET" | "POST" | "PATCH" | "PUT" | "DELETE";

export interface IResponse<R> {
  result: R | null;
  status: number;
  error: Error | null;
  message: string | null;
}

export const baseFetch = async <P, R>(
  url: string,
  params: P,
  method: IHttpMethods = "GET",
  headers: {[key: string]: string} = {},
  noResponse: boolean,
): Promise<IResponse<R>> => {
  const body = method !== "GET" ? {body: JSON.stringify(params)} : {};

  const hasParams = Object.keys(params).length > 0;
  const urlResult = method !== "GET" ? `${url}` : `${url}${hasParams ? "?" : ""}${querystring.stringify(params)}`;

  try {
    const res = await fetch(urlResult, {
      method,
      ...body,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        ...headers,
      },
    });

    let json = {};

    try {
      json = !noResponse ? await res.json() : {};
    } catch (err) {
      if (noResponse) {
        return {
          result: null,
          status: res.status,
          error: null,
          message: null,
        };
      } else {
        return {
          result: null,
          status: res.status,
          error: new Error(res.statusText),
          message: res.statusText,
        };
      }
    }
    // const json = !noResponse ? await res.json() : {};
    const status = res.status;

    return {result: json as R, status, error: null, message: null};
  } catch (error) {
    return {
      result: null,
      status: 500,
      error: error as Error,
      message: error.message,
    };
  }
};
