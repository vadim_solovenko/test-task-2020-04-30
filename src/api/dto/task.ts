export interface ITask {
  amount: number;
  organization: string;
  inn: string;
  status: string[];
  client: string;
  clientStatus: string[];
  number: string;
  date: string;
}

export interface ITaskRequest {}

export interface ITaskResponse {
  data: ITask[];
}

export interface ITaskFilter {
  taskNumber?: string;
  client?: string;
}
