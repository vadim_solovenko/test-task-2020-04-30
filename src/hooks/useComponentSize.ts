import {RefObject, useState, useCallback, useLayoutEffect} from "react";

export interface IComponentSize {
  width: number;
  height: number;
}

const getSize = <T>(el: HTMLElement | null): IComponentSize => {
  if (!el) {
    return {
      width: 0,
      height: 0,
    };
  }

  return {
    width: el.offsetWidth,
    height: el.offsetHeight,
  };
};

export const useComponentSize = (ref: RefObject<HTMLElement>, callback?: (size: IComponentSize) => void) => {
  const [componentSize, setComponentSize] = useState(() => getSize(ref.current));

  const handleResize = useCallback(() => {
    if (ref.current) {
      setComponentSize(getSize(ref.current));
    }
  }, [ref]);

  useLayoutEffect(() => {
    if (!ref.current) {
      return;
    }

    handleResize();

    window.addEventListener("resize", handleResize);
    window.addEventListener("orientationchange", handleResize);

    return function () {
      window.removeEventListener("resize", handleResize);
      window.removeEventListener("orientationchange", handleResize);
    };
  }, [handleResize, ref]);

  if (callback) {
    callback(componentSize);
  }

  return componentSize;
};
