import styled from "astroturf";
import React, {useEffect, useRef, useState} from "react";
import {ITask} from "../../api/dto/task";
import {useComponentSize} from "../../hooks/useComponentSize";
import {usePrevious} from "../../hooks/usePrevious";
import {useWindowSize} from "../../hooks/useWindowSize";
import {Tag} from "../../ui/Tag";
import Checkbox from "@material-ui/core/Checkbox";

interface IProps {
  data: ITask;
}
export const Task = ({data}: IProps) => {
  const ref = useRef(null);
  const {height} = useComponentSize(ref);
  const [startHeigth, setStartHeight] = useState<number | undefined>();

  const {width} = useWindowSize();
  const prevWidth = usePrevious(width);

  useEffect(() => {
    if (height && (!startHeigth || (width && prevWidth && width !== prevWidth))) {
      setStartHeight(height);
    }
  }, [height, startHeigth, width, prevWidth]);

  const {amount, organization, inn, status, client, clientStatus, number, date} = data;

  return (
    <RelativeContainer style={startHeigth ? {height: startHeigth} : {}}>
      <Container ref={ref}>
        <PaddingContainer>
          <TitleContainer>
            <Title>Проверить данные клиента</Title>
            <CheckBoxContainer>
              <Checkbox />
            </CheckBoxContainer>
          </TitleContainer>
          <Amount>{`${amount} руб.`}</Amount>
          <Organization>{organization}</Organization>
          <Inn>{`ИНН ${inn}`}</Inn>
        </PaddingContainer>

        <Hidden>
          <Tags>
            {status.map((tag) => (
              <Tag key={tag}>{tag}</Tag>
            ))}
          </Tags>

          <Client>{client}</Client>

          <Tags last>
            {clientStatus.map((tag) => (
              <Tag key={tag}>{tag}</Tag>
            ))}
          </Tags>
        </Hidden>

        <PaddingContainer>
          <BottomRow>
            <BottomText>{number}</BottomText>
            <BottomText>{`от ${date}`}</BottomText>
          </BottomRow>
        </PaddingContainer>
      </Container>
    </RelativeContainer>
  );
};

const Hidden = styled.div`
  max-height: 0;
  overflow: hidden;
  opacity: 0;
  transition-duration: 500ms;
  transition-property: max-height, opacity;
`;

const CheckBoxContainer = styled.div`
  transition-duration: 500ms;
  transition-property: max-height, opacity;
  max-height: 0;
  opacity: 0;
  margin-top: -10px;
`;

const Container = styled.div`
  background-color: #fff;
  border-radius: 10px;
  cursor: pointer;

  padding: 16px 0;
  position: absolute;
`;

const RelativeContainer = styled.div`
  position: relative;

  &:hover {
    ${Container} {
      z-index: 1;
      box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
    }

    ${Hidden} {
      opacity: 1;
      max-height: 300px;
    }

    ${CheckBoxContainer} {
      max-height: 50px;
      opacity: 1;
    }
  }
`;

const PaddingContainer = styled.div`
  padding: 0 24px;
`;

const TitleContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

const Title = styled.div`
  font-size: 18px;
  font-weight: bold;
  color: #2e3351;
  margin-right: 10px;
`;

const Amount = styled.div`
  font-size: 14px;
  margin-top: 4px;
  font-weight: bold;
`;

const Organization = styled.div`
  font-size: 14px;
  margin-top: 4px;
`;

const Inn = styled.div`
  color: #c8c8c8;
  font-size: 12px;
  margin-top: 4px;
  font-weight: bold;
`;

const Tags = styled.div<{last?: boolean}>`
  display: flex;
  flex-wrap: wrap;
  padding: 8px 24px 16px;

  & > div {
    margin-top: 8px;
  }

  & > :not(:last-child) {
    margin-right: 8px;
  }

  &.last {
    padding-bottom: 4px;
  }
`;

const Client = styled.div`
  background-color: #f8ecfa;
  padding: 10px 24px;
  font-size: 16px;
  font-weight: 600;
`;

const BottomRow = styled.div`
  display: flex;
  margin-top: 10px;
  & > :not(:first-child) {
    margin-left: 24px;
  }
`;

const BottomText = styled.div`
  color: #c8c8c8;
  font-weight: bold;
  font-size: 12px;
`;
