import styled from "astroturf";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import serachIconSrc from "../../assets/serachIcon.svg";
import {TaskAction} from "../../store/tasks/action";
import {TaskActionAsync} from "../../store/tasks/actionAsync";
import {TaskSelectors} from "../../store/tasks/selectors";
import {Task} from "./Task";

export const Tasks = () => {
  const {content} = useSelector(TaskSelectors.items);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(TaskActionAsync.getList({}));
  }, [dispatch]);

  const onChangeNumber = (e: any) => {
    dispatch(TaskAction.setFilter({taskNumber: e.target.value}));
  };

  const onChangeName = (e: any) => {
    dispatch(TaskAction.setFilter({client: e.target.value}));
  };

  return (
    <Container>
      <Filter>
        <InputContainer>
          <FilterInput placeholder="Номер заявки" onChange={onChangeNumber} />
          <SearchIconContainer>
            <img src={serachIconSrc as string} alt="search" />
          </SearchIconContainer>
        </InputContainer>

        <InputContainer>
          <FilterInput placeholder="Наименование клиента" onChange={onChangeName} />
          <SearchIconContainer>
            <img src={serachIconSrc as string} alt="search" />
          </SearchIconContainer>
        </InputContainer>
      </Filter>
      <ContentContainer>
        {content ? content.map((task) => <Task data={task} key={`${task.number}`} />) : null}
      </ContentContainer>
    </Container>
  );
};

const Container = styled.div`
  box-sizing: border-box;
  padding: 24px;
  width: 100%;
`;

const FilterInput = styled.input`
  color: #c8c8c8;
  border: 1px solid #c8c8c8;
  border-radius: 12px;
  padding: 10px 30px 10px 40px;
  outline: none;
  &::placeholder {
    color: #c8c8c8;
  }
`;

const InputContainer = styled.div`
  position: relative;
`;

const SearchIconContainer = styled.div`
  position: absolute;
  top: 0;
  width: 20px;
  height: 100%;
  left: 10px;
  display: flex;
  & > img {
    width: 100%;
  }
`;

const Filter = styled.div`
  display: flex;
  ${InputContainer} + ${InputContainer} {
    margin-left: 20px;
  }
`;

const ContentContainer = styled.main`
  margin-top: 24px;
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-column-gap: 24px;
  grid-row-gap: 24px;

  @media screen and (max-width: 1440px) {
    grid-template-columns: repeat(4, 1fr);
  }

  @media screen and (max-width: 1280px) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media screen and (max-width: 850px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media screen and (max-width: 600px) {
    grid-template-columns: repeat(1, 1fr);
  }
`;
