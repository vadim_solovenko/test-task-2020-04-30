import {AsyncActionCreators} from "typescript-fsa";
import {baseFetch, IHttpMethods} from "../api/baseFetch";
import {interceptor, queryRaceInit, queryRaceSuccess} from "./interceptor";
import {IAppState, IThunkAction} from "./mainReducer";

export interface IFetchParams<P, R> {
  params: P;
  url: string;
  method: IHttpMethods;
  headers?: {[key: string]: string};
  actions: AsyncActionCreators<P, R, Error>;
  onSuccess?: (getState: () => IAppState, result?: R) => void;
  transformData?: (result: R, getState: () => IAppState) => R;
  onFail?: () => void;
  noResponse?: boolean;
  noSendParams?: (keyof P)[];
  noErrorToast?: boolean;
  useQueryRace?: boolean;
  getErrorMessage?: (message: string) => string;
}

export const callApi = <P, R>({
  params: rawParams,
  url,
  method,
  headers,
  actions,
  onSuccess,
  onFail,
  noResponse = false,
  transformData,
  noSendParams,
  noErrorToast,
  useQueryRace = true,
  getErrorMessage = (message) => message,
}: IFetchParams<P, R>): IThunkAction => {
  return async (dispatch, getState, {history}) => {
    const {sendParams, params} = interceptor(rawParams, noSendParams);
    const key = useQueryRace ? queryRaceInit(actions) : "";

    dispatch(actions.started(sendParams));

    const {result, status, message} = await baseFetch<P, R>(url, params, method, headers, noResponse);

    if (queryRaceSuccess(actions, key)) {
      if (status >= 400 || result == null) {
        const error = {
          name: status.toString(),
          message: ({...result} as Error).message || message || status.toString(),
        };

        if (status === 502) {
          history.push("/404");
        }

        dispatch(actions.failed({params: sendParams, error}));
        onFail && onFail();
      } else {
        const transformedResult = transformData ? transformData(result, getState) : result;
        dispatch(actions.done({params: sendParams, result: transformedResult}));
        onSuccess && onSuccess(getState, transformedResult);
      }
    }
  };
};
