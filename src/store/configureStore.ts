// import {AsyncStorage} from "react-native";
import {History} from "history";
import {applyMiddleware, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import {persistStore, persistReducer} from "redux-persist";
import thunk from "redux-thunk";
import {mainReducer} from "./mainReducer";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web

const composeEnhancers = composeWithDevTools({
  shouldHotReload: true,
});

const persistConfig = {key: "root", storage, whitelist: [""]};

export function configureStore(callback: () => void, history: History<any>) {
  const middleware = [thunk.withExtraArgument({history})];
  const enhancer = composeEnhancers(applyMiddleware(...middleware));
  const reducer = persistReducer(persistConfig, mainReducer);

  const store = createStore(reducer, enhancer);
  const persistor = persistStore(store);

  return {store, persistor};
}
