import {History} from "history";
import {Action, combineReducers} from "redux";
import {ThunkAction, ThunkDispatch} from "redux-thunk";
import {ITaskState, TaskReducer} from "./tasks/reducer";

export const mainReducer = combineReducers({
  tasks: TaskReducer,
});

export interface IAppState {
  tasks: ITaskState;
}

export interface IAppDispatch extends ThunkDispatch<IAppState, Error, Action> {}

export interface IThunkAction
  extends ThunkAction<
    Promise<void>,
    IAppState,
    // tslint:disable-next-line:no-any
    {history: History<any>},
    Action
  > {}
