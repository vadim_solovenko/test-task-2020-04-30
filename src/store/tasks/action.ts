import {actionCreatorFactory} from "typescript-fsa";
import {ITaskRequest, ITaskResponse, ITaskFilter} from "../../api/dto/task";

const ActionFactory = actionCreatorFactory("TASKS");

export const TaskAction = {
  getList: ActionFactory.async<ITaskRequest, ITaskResponse, Error>("GET_LIST"),
  setFilter: ActionFactory<ITaskFilter>("SET_FILTER"),
};
