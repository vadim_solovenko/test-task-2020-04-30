import {ITaskRequest} from "../../api/dto/task";
import {callApi} from "../apiActionsAsync";
import {IThunkAction} from "../mainReducer";
import {TaskAction} from "./action";

export const TaskActionAsync = {
  getList: (params: ITaskRequest): IThunkAction => {
    return callApi({
      url: "http://www.mocky.io/v2/5eaabc592d0000812a268918",
      method: "GET",
      params,
      actions: TaskAction.getList,
    });
  },
};
