import {reducerWithInitialState} from "typescript-fsa-reducers";
import {ITask, ITaskFilter} from "../../api/dto/task";
import {ContentLoading, initialContentLoading} from "../../interfaces/ContentLoading";
import {TaskAction} from "./action";

export interface ITaskState {
  items: ContentLoading<ITask[]>;
  filter: ITaskFilter;
}

const initialState: ITaskState = {
  items: initialContentLoading([]),
  filter: {},
};

export const TaskReducer = reducerWithInitialState(initialState)
  .case(TaskAction.getList.started, (state) => ({
    ...state,
    items: {
      ...state.items,
      status: "loading",
    },
  }))
  .case(TaskAction.getList.done, (state, {result}) => ({
    ...state,
    items: {
      status: "loaded",
      content: result.data,
    },
  }))
  .case(TaskAction.getList.failed, (state, {error}) => ({
    ...state,
    items: {
      content: [],
      status: "loaded",
      error,
    },
  }))
  .case(TaskAction.setFilter, (state, filter) => ({
    ...state,
    filter: {...state.filter, ...filter},
  }));
