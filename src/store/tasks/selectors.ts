import {createSelector} from "reselect";
import {IAppState} from "../mainReducer";

export const TaskSelectors = {
  items: createSelector(
    (state: IAppState) => state.tasks.items,
    (state) => state.tasks.filter.client,
    (state) => state.tasks.filter.taskNumber,
    (items, clientName, taskNumber) => ({
      ...items,
      content: items.content.filter(
        ({client, number}) =>
          (!clientName || client.toLowerCase().includes((clientName || "").toLowerCase())) &&
          (!taskNumber || number.toLowerCase().includes((taskNumber || "").toLowerCase())),
      ),
    }),
  ),

  filter: (state: IAppState) => state.tasks.filter,
};
