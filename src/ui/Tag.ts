import styled from "astroturf";

export const Tag = styled.div`
  padding: 2px 16px;
  text-transform: uppercase;
  border-radius: 50px;
  background-color: #e7ceec;
  color: #fff;
`;
